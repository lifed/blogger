interface Project {
  title: string
  description: string
  href?: string
  imgSrc?: string
}

const projectsData: Project[] = [
  {
    title: '空作品',
    description: `好像是作品的文章，實際上什麼都不存在`,
    // imgSrc: 'https://i.imgur.com/imyovey.jpeg',
    // href: 'https://www.google.com',
  },
]

export default projectsData
