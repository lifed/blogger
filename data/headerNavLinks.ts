const headerNavLinks = [
  { href: '/', title: '根目錄' },
  { href: '/blog', title: '文章' },
  { href: '/tags', title: '標籤' },
  { href: '/projects', title: '作品集' },
  { href: '/about', title: '不要好奇' },
]

export default headerNavLinks
