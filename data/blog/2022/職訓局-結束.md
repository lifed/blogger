---
title: 職訓局_結束
date: 2022-07-01 11:09:50
tags: ['生活']
---
## 結業證書
老師會恐嚇說沒交沒分數可能會發生什麼事情，但...其實也不會還是能安逸的拿到結業證書。

## 分數
Q：分數有很重要嗎？    
A：沒有，但每次準時繳作業，若第一名可以得到一份精美的獎品加獎狀。  
Prise：保溫杯。  

## 全勤
Q：全勤獎有獎勵嗎？  
A：有，每梯次不一樣。  
Prise：泡咖啡器具。

## 職務
通常會有兩個職務，班代與學藝，而期末只會發獎品給班代。

## 電子商務網站建置
內容由五個老師分別指導
1. 證照
2. html, css, js, php, mysql, XAMPP
3. adobe {illusion, ae}, figma
4. js
5. adobe {illusion, ps}

### 證照
丙級網頁設計技術士、乙級技術士電腦軟體應用。

完全沒必要的課程，公家單位自爽用的課程，浪費時間。  

網頁設計丙級：  
2022年用Dreamweaver CS6寫程式，您敢信？  
2022年用Photo impact、photoshop cs6，嚇死我啦。  
2022年用50版的chrome去瀏覽頁面的正確性，我的老天鵝阿！

軟體應用乙級：
或許行政人員有用到，但...課程是電子商務網站建置喔...。  

請問職訓局可以找到哪一家公司願意收有網丙證照的人寫網頁程式嗎？  

### 設計類課程
A老師, Adobe {Illustrator, After Effects}, figma  
B老師, Adobe {Illustrator, Photoshop}  

A老師會實做但教的不順暢。  
B老師教的比較順，如果認真學可以學到工具上使用的細節，上課節奏拿捏的很好。
 
整體來講設計類有學到滿多工具上使用的技巧。  

### 程式類
前台: html, css(sass,scss), js(jquery)  
後台: php, mysql  
其他：dreamweaver、英打

工具：vscode

相較之下程式設計課是讓人失望的，教學流程凌亂，每個老師分開教，A老師都會問說B老師跟C老師有教過嗎？那我不教囉？？？   

html好像沒什麼好說的  
css基本概念都有點過，但沒有展現sass, scss迷人的地方  
js基本迴圈變數講解  
jquery大概會懂得$.ajax... 大概吧...XD    
php迴圈變數講解  
mysql 基本crud，但無講解關聯（照抄就對了）  

課程內容給一份ppt內容照著把程式打出來，順便debug一下ppt的程式內容是否錯誤。  

整體來講我是沒在上課(被毆)。  

後面的日子都專注前後分離(vue & spring boot)的學習，即便是同學有問題問我也是可以解決的，這跟jsp差不多。  

## 結論
如果想從這門課學到寫程式維生那...理論上不太可能，除非自學能力很足，那...好像也不用來上這門課。

另外是...如果要上課也想找男女朋友可以多多參加課程呦 > , ^