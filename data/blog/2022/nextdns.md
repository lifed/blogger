---
title: nextdns
date: 2022-07-28 21:05:25
tags: ['網路']
---

## 前言

從pi-hole到NextDNS，從自己架設變成用別人的伺服器，確實著實便利，

但...我想您得知道，免費是要付出自己的隱私權。

即便如此讓NextDNS擋掉大部分的隱私擷取的網站或惡意網站等，要來的好。

## NextDNS

### 注意

請不要搭配IPv6使用，除非您的除錯技巧非常厲害。

請不要以為設定好後以為無後顧之憂，手機的App可能會因此異常，需要設定白名單。

### 安全
建議全部啟用

### 隱私 - > 阻止列表

1. NextDNS 推薦的廣告和跟蹤器阻止列表
2. EasyList China

### 白名單

``` text
## app異常 line的today 
*.googletagmanager.com

## app異常 line的「Today看世界」影片含廣告會無法播放
## 注意：會變得無法擋廣告
*.g.doubleclick.net

## line無法正常傳遞影音檔案
*.line.me
*.line-scdn.net
*.line-apps.com
*.linecorp.com
```

### 設定 
建議全部啟用

### 路由器端設定
記得先匯入憑證，但照著步驟可能會有問題！  
您可以先用瀏覽器抓憑證下來，拖曳檔案到Winbox的file中再匯入憑證。
![set1](https://i.imgur.com/6frxnbO.jpg)
1. **綠色**的地方請放對位置，記得是放您的ID不是我的...！  
2. **藍色**若要讓NextDNS辨識機器名稱，請在藍色的部份加入您要的名稱。  
3. **紅色**請照抄，設定32也可以24也行(看不懂請忽略)。
![set2](https://i.imgur.com/ixbISAb.jpg)

### 問題
#### Line(IPv6)
注意：同時開啟IPv6與IPv4，網路會優先走IPv6。  

猜測NextDNS可能沒有正確解析Line的DNS，導致一些異常。  

## 後記
IPv6前前後後開了兩三次，都會衍生額外問題但又不得不學習，真是很尷尬。
