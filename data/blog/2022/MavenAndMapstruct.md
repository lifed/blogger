---
title: Mapstruct兩三事
date: 2022-10-11 14:01:44
tags: ['技術']
---
**文章內容還在精進中，可能有大量錯誤，望請海涵**

## Mapstruct
[Mapstruct Reference Guide](https://mapstruct.org/documentation/stable/reference/pdf/mapstruct-reference-guide.pdf)
### MapStruct Support IntelliJ plugin
推薦安裝，在IDE能更精確的達成一切功能而不是顯示紅字。  

@Mapping下的Entity有extend如BaseEntity，關聯的時候會無法正確Go to Declaration。  

註：文件目錄 2.6 IDE Integration

### MapperImpl
[官方文檔](https://mapstruct.org/documentation/installation/)  

MapperImpl是透過Maven的compile去產生的，一般可直接compile。若有懷疑可先clean後compile。

如果compile未產生任何Impl代表maven內的設定檔錯誤(通常是沒設定)。

**像是透過Intellij IDEA的外掛JPA Buddy所產生的Dto與Mapper並不會修改maven的設定檔。**

### 樹狀遞迴
資料庫
![Imgur](https://i.imgur.com/UOpf3z2.jpg)

透過jpa buddy產生dto與mapper，**記得還要巢狀類別內加上List才遞迴撈取**
![Imgur](https://i.imgur.com/ovLWWSX.png)

記得要透過maven的compile產出impl
```java
@Data
@AllArgsConstructor
@NoArgsConstructor
// DTO的物件上面輸出json格式可直接忽略null
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StoreCategoryResponse {
    private Long id;
    private String name;
    private List<ModuleCategoryDto> moduleCategories;
    
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ModuleCategoryDto {
        private Long id;
        private String name;
        // 遞迴撈取需自行加這行 VVV
        private List<ModuleCategoryDto> moduleCategories;
        // 遞迴撈取需自行加這行 ^^^
    }
}

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface StoreSharedMapper {
    StoreCategory storeCategoryDtoToStoreCategory(StoreCategoryDto storeCategoryDto);

    StoreCategoryDto storeCategoryToStoreCategoryDto(StoreCategory storeCategory);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    StoreCategory updateStoreCategoryFromStoreCategoryDto(StoreCategoryDto storeCategoryDto, @MappingTarget StoreCategory storeCategory);

    @AfterMapping
    default void linkStoreCategories(@MappingTarget StoreCategory storeCategory) {
        storeCategory.getStoreCategories().forEach(theStoreCategory -> storeCategory.setStoreCategory(storeCategory));
    }
}
```
test code
```java
public class queryTest {
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    void queryTest() {
        // 撈root資料 parent_id is null
        List<StoreCategory> storeCategories = storeCategoryRepository.getTree();
        List<StoreCategoryDto> storeCategoryResponses = new ArrayList<>();
        for (StoreCategory storeCategory : storeCategories) {
            storeCategoryResponses.add(storeSharedMapper.storeCategoryToStoreCategoryDto(storeCategory));
        }
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            System.out.println(objectMapper.writeValueAsString(storeCategoryResponses));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
```
結果
```json
[
  {
    "id": 1,
    "name": "中式美食",
    "moduleCategories": [
      {
        "id": 4,
        "name": "第二層類別362",
        "moduleCategories": [
          {
            "id": 350,
            "name": "第三層類別333",
            "moduleCategories": [
              {
                "id": 351,
                "name": "第四層類別444",
                "moduleCategories": []
              }
            ]
          }
        ]
      },
      {
        "id": 7,
        "name": "第二層類別372",
        "moduleCategories": []
      }
    ]
  },
  {
    "id": 31,
    "name": "日式美食",
    "moduleCategories": []
  }
]
```

### Lombok
#### @SuperBuilder 大小寫問題
未使用前正常
![Imgur](https://i.imgur.com/AtV6FMG.jpg)
使用後沒有正確產生
![Imgur](https://i.imgur.com/D8prx7z.jpg)
修改變數名稱開頭為小寫，即正確產生。  
![Imgur](https://i.imgur.com/7YpIqOG.jpg)

### 問題
#### cannot find symbol
似乎從maven-compiler-plugin某版本(3.1)之後開始會出現這個錯誤訊息？  

主因是使用lombok所導致的，mapstruct已經有提出範例解了。  

除了mapstruct-processor與lombok之外請務必加上lombok-mapstruct-binding不然又會導致另外的錯誤。

[Lombok/Mapstruct problem: Cannot find symbol](https://github.com/mapstruct/mapstruct/issues/1270)

#### ModuleCategory的問題(待解決)
因為該List放入的是ModuleCategory所以在遞迴撈取的時候，並未區分類別(module_name)。

**簡單講sql沒有產生where module_name = 'categories'**

```text 
private List<ModuleCategory> moduleCategories;
```


