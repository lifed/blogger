---
title: GeForceNow
date: 2020-10-09 20:57:15
tags: ['遊戲']
---
## 前言
很可惜GeForce Now出走了一堆遊戲，算盤都變成了空撥。  

不過稍微試用一下很訝異的一點是滑鼠真的會跟著動，意思是「感受到即時」。  

## Game
### 不錯的遊戲體驗
1. This War of Mine 
2. The Witcher 3: Wild Hunt

### Path of Exile 國際服
No way.
主機在台灣，以至於Steam在偵測的時候直接顯示此區域無法下載。  

## Issues
### 特殊架構無法連線?
網路與主機架構  
小烏龜 -(PPPoE)- 分享器 -(NAT)- MyPC -(NAT) - VM  
推測問題
1. VM的問題
2. Double Nat

![img](https://i.imgur.com/A6PRVSa.jpg)

架構變更
小烏龜 -(PPPoE)- 分享器 -(NAT)- MyPC -(NAT|Bridge) - VM  

目前似乎解決？(都是THEY的錯啦！)

### Win10 萬惡輸入法
注音輸入法會導致一些遊戲跳視窗的問題，如巫師三。    
  
目前解決方案請新增英文語系。  

另外如果anydesk不能用ctrl+vcx等組合鍵，沒錯也是注音的問題。  