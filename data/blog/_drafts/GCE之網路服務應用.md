---
title: GCE之網路服務應用
date: 2017-10-01 18:37:17
tags: [cloud,network]
---
前言  
不怎麼新鮮的東西但值得一提。

# 環境建制
沿用Server on GCE那篇文章的虛擬機，那麼會有些問題看看怎麼解決囉。  

## 升級核心

    # uname -r
    3.0X.XX的版本號

升級核心必要軟體  

    # apt-get install firmware-linux-free irqbalance

新增安裝源，這部份是為了linux-base。(預設好像是3.5?什麼鬼的，要安裝最新的核心必須要4.3以上)
    # vim /etc/apt/sources.list

    ...
    # Stable backports
    deb http://ftp.debian.org/debian jessie-backports main contrib non-free
    deb-src http://ftp.debian.org/debian jessie-backports main contrib non-free
    ...

    # apt-get update
    # apt-get -t jessie-backports install linux-base

    ## 也許您該先找找版本是不是存在!
    ## apt-cache search linux-image  

    # apt-get install linux-image-4.9.0-0.bpo.2-amd64

## 設定bbr

    # vim /etc/modules-load.d/modules.conf

    ...
    tcp_bbr
    ...

    # vim /etc/sysctl.conf

    ...
    net.core.default_qdisc=fq
    net.ipv4.tcp_congestion_control=bbr
    ...

    # reboot

## GCP開port
![port](https://i.imgur.com/Vk8FP9B.jpg)

## 確認
    # uname -r
    Version >= 4.9

    # lsmod | grep bbr
    tcp_bbr
    # sysctl net.ipv4.tcp_congestion_control
    net.ipv4.tcp_congestion_control = bbr
    # sysctl net.core.default_qdisc
    net.core.default_qdisc = fq
    # sysctl net.ipv4.tcp_available_congestion_control
    net.ipv4.tcp_available_congestion_control = bbr cubic reno

# ShadowSocksR

## 安裝相依軟體
    # apt-get install python-pip
    # pip install --upgrade pip
    # pip install cymysql

## 主菜

    ## apt-get install git // if git not found
    # git clone -b manyuser https://github.com/shadowsocksr-backup/shadowsocksr.git
    # cd shadowsocksr/
    # bash initcfg.sh
    # vim user-config.json

看起來像是這樣  
![setting](https://i.imgur.com/nonBHBj.jpg)


    # cd shadowsocks
    # python server.py -d start

# 連接ShadowSocksR

![speedtest](https://i.imgur.com/j9ucw52.jpg)

## win用戶端
[SSR下載](https://github.com/shadowsocksr-backup/shadowsocksr-csharp/releases)
![winset](https://i.imgur.com/ASgmC3V.jpg)

## linux用戶端
    $ git clone -b manyuser https://github.com/shadowsocksr-backup/shadowsocksr.git
    $ cd ./shadowsocksr/shadowsocks
    $ sudo python local.py -b 127.0.0.1 -l 1080 -s "serverIPADRESS" -p "serverport" -k "serverpassword" -m "aes-256-cfb" -O "auth_sha1_v4" -o "tls1.2_ticket_auth" -G "" -g "" -d start

    sudo python local.py //您確定真的要我解釋？
    -b 127.0.0.1 -l 1080 //本地端開給瀏覽器的代理
    -s "serverIPADRESS" -p "8388" -k "pass" //架設SSR伺服器的IP, port與密碼
    -m "aes-256-cfb" -O "auth_sha1_v4" -o "tls1.2_ticket_auth" //加密 協議 混淆
    -G "" -g "" // 協議參數 混淆參數
    -d start //背景執行

開啟瀏覽器並設定代理!


# 後記
使用此軟體，看實況什麼的都不會頓了，各種愛情動作台都可以爽爽看！  

中華有時候走得route真的爛到爆，透過google的線路去抓回來根本神。  

此軟體使用sock5所以並不會影響到遊戲連接的ip，看網頁sock5、玩遊戲原本ip，讚讚讚。

如果GCP的cpu使用在低一點會更不錯吧(價格的部份)。
![halfmonth](https://i.imgur.com/eJGWwFv.jpg)  
