---
title: Manifest設定集
date: 2017-01-08 23:41:55
tags: android
---

# 前言
軟體: Android Studio 2.2.3

# 切換啟動頁面
當新增activity且要成為初始啟動頁面，須在其他activity裡面移至想要的activity裡。

    <intent-filter>
        <action android:name="android.intent.action.MAIN" />
        <category android:name="android.intent.category.LAUNCHER" />
    </intent-filter>

如下

    <activity
        android:name=".LoginActivity_SomeActivity"
        android:label="@string/title_activity_login">
        <intent-filter>
            <action android:name="android.intent.action.MAIN" />
            <category android:name="android.intent.category.LAUNCHER" />
        </intent-filter>
    </activity>

## 新增啟動頁面
![Imgur](http://i.imgur.com/IIKrKiE.jpg)
