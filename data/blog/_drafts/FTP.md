---
title: "Vsftpd架設"
date: 2015-03-01 22:26:00
tags:
    - ftp
---

## 參考

[Arch的vsftpd教學](https://wiki.archlinux.org/index.php/Very_Secure_FTP_Daemon)  
通常我都不推薦非英文的教學，其他的地區常常沒有滾到最新。
[DEFAULT config](http://vsftpd.beasts.org/vsftpd_conf.html)

## 設定

### 主要檔案

    ``` bash
    /etc/vsftpd.conf ## ftp的設定檔
    /etc/vsftpd/.passwd ## 虛擬用戶的帳密
    /etc/pam.d/vsftpd ## pam
    ```

### 設定

    ## vim /etc/vsftpd.conf


    ## global
    ## listen=YES
    use_localtime=YES
    local_umask=022
    write_enable=YES
    local_enable=YES
    local_max_rate=15360 #這個才15KB 自己...嗯
    anonymous_enable=NO

    ## 虛擬帳號設定
    hide_ids=YES
    guest_enable=YES
    guest_username=virtual
    virtual_use_local_privs=YES
    pam_service_name=vsftpd
    #可自行新增其他使用者名稱，範例為"virtual"
    nopriv_user=virtual

    ## 多重虛擬帳號資料夾設定
    ## 如果沒有新增該帳號的資料夾，則會無法登入(有設定chroot的話)。
    local_root=/somewhere/$USER
    user_sub_token=$USER

    ## passive port
    pasv_min_port=40000
    pasv_max_port=50000

    ## chroot jail 把帳號鎖在自己的目錄(安全性設定)
    chroot_local_user=YES
    #chroot_list_enable=YES
    #chroot_list_file=/etc/vsftpd/chroot_list
    allow_writeable_chroot=YES

    ## dual log
    dual_log_enable=YES
    xferlog_enable=YES
    xferlog_file=/var/log/xferlog
    vsftpd_log_file=/var/log/vsftpd.log

    ## ftp(s)
    ssl_enable=YES
    #allow_anon_ssl=NO
    force_local_data_ssl=NO
    force_local_logins_ssl=NO
    ssl_tlsv1=YES
    ssl_sslv2=NO
    ssl_sslv3=NO
    rsa_cert_file=/etc/ssl/certs/vsftpd.pem

    #重點！！ftps的重點！(for routeros!!)
    #錯誤-->回應: 227 Entering Passive Mode (LAN IP,port).
    #正確-->回應: 227 Entering Passive Mode (真實IP,port).

    pasv_addr_resolve=YES
    pasv_address=Static IP or DDNS
    #衍生問題當重新開機會無法正確啟動，因為網路服務與FTP啟動順序的問題。
    #要自行設定dhcpd@[eth interface].service


    ## vim /etc/pam.d/vsftpd
    (vsftpd的名稱可以自訂相對的vsftpd.conf的內容須修改)


    auth required pam_pwdfile.so pwdfile /etc/vsftpd/.passwd
    account required pam_permit.so


    ## vim /etc/vsftpd/.passwd
    如果沒有該檔案第一次要加-c再來就不要（也可以試著都加加看）


    ## htpasswd -cd /etc/vsftpd/.passwd user1
    ## htpasswd -d /etc/vsftpd/.passwd user{2-9999}

## 自習題

Q. htpasswd -d是採用什麼加密？  
Q. pam.d的vsftpd設定檔，這樣的設定又是採取什麼方式加(解？)密？  
