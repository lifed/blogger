---
title: samba二三事
date: 2016-04-09 23:06:09
tags:
    - samba
---
[Arch samba](https://wiki.archlinux.org/index.php/samba)  

## quick configure

    ## cp /etc/samba/smb.conf.default /etc/samba/smb.conf

## global

    workgroup = WORKGROUP

### if you do not want this

    ;[printers]
    ;   comment = All Printers
    ;   path = /var/spool/samba
    ;   browseable = no
    ## Set public = yes to allow user 'guest account' to print
    ;   writable = no
    ;   printable = yes

## share definitions

    [homes]
       comment = Home Directories
       ;   guest ok = no
       valid users = %S
       browseable = no
       writable = yes

%S = 誰登入就是掛誰的名子  
@group = 在group裡面都可以登入  
當然也可以直接掛名 請參考Mary's and Fred's stuff的範例。
[注意]：可以登入並不代表可以修改，自行給予chmod權限。
[注意]：homes代表的是自己的家目錄，會把你的A-Z磁碟都「丟人現眼」。

## set samba users
samba使用者跟linux使用者是被區分的，samba必須另行建立。

    ##  pdbedit -a -u samba_user

## testparm

    ## testparm -s

失敗了就再嘗試不同的設定吧，至少我是這樣快速設定一下samba。

## 參照連結(mklink)
有些程式無法從samba(\\domain)開啟，那麼換個方式讓它假實體化就得啦！其實就像linux四處亂mount一樣。  

    mklink /D "\\2.2.2.94\SAMBA" "C:\Users\ADMIN\Desktop\samba"

### Samba & VHD
徹底解決網路磁碟執行程式會發生的問題
<iframe width="560" height="315" src="https://www.youtube.com/embed/labK2DjgBLU" frameborder="0" allowfullscreen></iframe>
