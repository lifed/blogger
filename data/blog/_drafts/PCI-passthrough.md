---
title: PCI passthrough
date: 2015-10-26 19:04:24
tags:
    - qemu
---
本篇已不更新！請參考 最新一篇的pci passthrough
最後更新日期：2016.07.11
# 前言
如果這段影片沒吸引到您，那跳過這篇文章吧！  
<iframe width="800" height="480" src="https://www.youtube.com/embed/37D2bRsthfI" frameborder="0" allowfullscreen></iframe>

本文基於ArchWiki的教學(此份教學隨時隨地都在變動)。  
[PCI passthrough via OVMF](https://wiki.archlinux.org/index.php/PCI_passthrough_via_OVMF)  

## 必要條件
1. 兩張顯示卡(內顯、外顯｜外顯、外顯)
2. BIOS虛擬技術必須開啟
3. [GPU passthrough](https://docs.google.com/spreadsheet/ccc?key=0Aryg5nO-kBebdFozaW9tUWdVd2VHM0lvck95TUlpMlE&usp=drive_web#gid=0) database by [noctavian](https://bbs.archlinux.org/profile.php?id=73114)

## 配備  
cpu: i7-4790  
ram: 24g  
gpu: hd4600(host), r9-390(guest)  

# 前置作業
開頭步驟做錯了後面一旦錯誤則會發生無法預期的結果！  
最常出現的錯誤搞錯那一張顯卡開機，請嚴加確認是否正確顯卡開機。  

1. BIOS虛擬化技術開啟
2. 強制HOST顯卡開機

# 安裝
## 安裝qemu軟體

    # yaourt -S qemu rpmextract

## 安裝rpmextract與ovmf

請上[Gerd Hoffman's repository](https://www.kraxel.org/repos/jenkins/edk2/)抓取虛擬機的BIOS。  
edk2.git-ovmf-x64-\*.rpm  
解壓縮該rpm檔案  

    # rpmextract edk2[tab][tab]
    # cp -R ./usr/share/* /usr/share

## 核心選擇
可使用原生核心！並非一定要安裝有補丁的核心，但是要分清楚版本差異使用的module不同。  
4.1以上用vfio-pci！  
4.1以下用pci-stub！  

    # yaourt -S linux-vfio
    # yaourt -S linux-vfio-lts

注意：請自行查詢此vfio的補丁(patch)成份。

# 設定
## 開啟 IOMMU

    # vim /etc/default/grub
    ...
    GRUB_CMDLINE_LINUX_DEFAULT=""
    GRUB_CMDLINE_LINUX=""
    ...

基本上設定GRUB_CMDLINE_LINUX_DEFAULT這行就對了！注意！這裡是intel，amd有另類參數。  

    GRUB_CMDLINE_LINUX_DEFAULT="intel_iommu=on intel_iommu=pt"

重新產生grub.cfg檔案  

    # grub-mkconfig -o /boot/grub/grub.cfg

### 重開機驗證是否開啟
如果有跳錯誤的話，請Google查詢。

    # dmesg|grep -e DMAR -e IOMMU
    ...
    [    0.000000] DMAR: IOMMU enabled
    ...
    # find /sys/kernel/iommu_groups/ -type l
    ...
    /sys/kernel/iommu_groups/0/devices/0000:00:00.0
    /sys/kernel/iommu_groups/1/devices/0000:00:01.0
    /sys/kernel/iommu_groups/2/devices/0000:00:02.0
    ...
    ...


## 區隔(隱匿)GPU
每個裝置都有特定的ID，此部份是要把它們找出來並且加以設定。  

    # lspci -nn|grep -iP "NVIDIA|Radeon|AMD|ATI"
    ##並用lspci -nn加以複查
    # lspci -nn

AMD的顯示卡有分Audio跟顯卡本身，這都要做區隔，從尾巴來看位置是1002:67b1(顯卡本身)與1002:aac8(顯卡音效)。

    01:00.0 VGA compatible controller [0300]: Advanced Micro Devices, Inc. [AMD/ATI] Hawaii PRO [Radeon R9 290] [1002:67b1] (rev 80)
    01:00.1 Audio device [0403]: Advanced Micro Devices, Inc. [AMD/ATI] Device [1002:aac8]

    # vim /etc/modprobe.d/vfio.conf
    ##請注意vfio_pci與pci-stub的差異
    ##新增下列參數
    options vfio-pci ids=1002:67b1,1002:aac8
    #options pci-stub ids=1002:67b1,1002:aac8

## 載入必要模組
啟動時載入必要模組。  

    # vim /etc/mkinitcpio.conf
    ...
    MODULES="vfio_pci vfio vfio_iommu_type1 vfio_virqfd"
    ...
    # mkinitcpio -p linux-vfio{,-lts}

## [for vfio]ACS Override Patch
使用vfio核心所以可以直接設定此參數，若原生核心可跳過此步驟。  

    # vim /etc/defualt/grub
    GRUB_CMDLINE_LINUX_DEFAULT="intel_iommu=on intel_iommu=pt pcie_acs_override=downstream"

重新產生grub.cfg檔案。  

    # grub-mkconfig -o /boot/grub/grub.cfg

## 額外參數設定
此參數是增加系統穩定性，減少BSOD出現機率。

    # vim /etc/modprobe.d/kvm-iommu.conf
    options kvm allow_unsafe_assigned_interrupts=1
    options vfio_iommu_type1 allow_unsafe_interrupts=1
    # for intel
    options kvm ignore_msrs=1
    options i915 enable_hd_vgaarb=1

## CPU效能
開機時把CPU區分開來，讓VM獨占幾個CPU，若此步驟不做效能肯定會受到影響。  
[After CPU pinning with libvirt, performance hasn't improved](https://www.redhat.com/archives/vfio-users/2015-September/msg00184.html)

    # vim /etc/defualt/grub
    GRUB_CMDLINE_LINUX_DEFAULT="... isolcpus=2,3,4,5,6,7 ..."

重新產生grub.cfg檔案。  

    # grub-mkconfig -o /boot/grub/grub.cfg  

# qemu
## 基本測試
測試顯卡與ovmf bios 是否正常執行。  
請注意這裡的-device vfio-pci,host=數字:數字.數字，請使用lspci -nn從開頭看起。  
這部份要改的地方就是倒數兩行的部份。一個是顯卡的位置參數、一個是UFEI BIOS的位置參數。

    qemu-system-x86_64 \
      -enable-kvm \
      -m 2048 \
      -cpu host,kvm=off \
      -vga none \
      -device vfio-pci,host=01:00.0 \
      -drive if=pflash,format=raw,readonly,file=/usr/share/edk2.git/ovmf-x64/OVMF_CODE-pure-efi.fd

若成功您的另一個螢幕應該就會出現BIOS開機視窗。
若無法啟動則往上捲動看那一部份是否有做錯。

## 實戰
[Windows 10 qemu/kvm guest win/fails...](http://ubuntuforums.org/showthread.php?t=2289210)  
裡面有完整範本底下就直接做解釋了。

### 處理器設定
    # socket=實體CPU數目(通常為1), cores=給予核心, threads=每顆核心給予幾個thread(通常為1|2)
    # smp = cores*threads
    # 註解那行可以自行體驗看看有什麼差別
    OPTS="$OPTS -cpu host -smp 6,sockets=1,cores=3,threads=2"
    #OPTS="$OPTS -cpu host -smp 6,sockets=1,cores=6,threads=1"
    OPTS="$OPTS -enable-kvm"

### Machine
使用q35可能會遇到其他衍生問題，建議是用預設。r9-390遇到的安裝驅動無限重開。  
    # 這裡不設定預設值為pc-i440fx
    #OPTS="$OPTS -M q35"

### 記憶體
    OPTS="$OPTS -m 12G"
    OPTS="$OPTS -mem-path /dev/hugepages"
    OPTS="$OPTS -mem-prealloc"
    OPTS="$OPTS -balloon none"

#### hugepage
[kernel Documentation](https://www.kernel.org/doc/Documentation/vm/hugetlbpage.txt)
[redhat](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/5/html/Tuning_and_Optimizing_Red_Hat_Enterprise_Linux_for_Oracle_9i_and_10g_Databases/sect-Oracle_9i_and_10g_Tuning_Guide-Large_Memory_Optimization_Big_Pages_and_Huge_Pages-Configuring_Huge_Pages_in_Red_Hat_Enterprise_Linux_4_or_5.html)

注意：此部份對效能有極大的影響！
設定公式"qemu設定的記憶體/2在多一點點"，如這裡是12G那就是1024*12/2+100，給太少會跳錯誤cannot allocate memory。  

    # vim /etc/fstab
    hugetlbfs /dev/hugepages  hugetlbfs mode=1770,gid=78  0 0

    # vim /etc/sysctl.d/40-hugepage.conf
    vm.nr_hugepages = 6244

transparent_hugepage預設是always這裡改成never似乎對效能比較有幫助？(radhat有稍微說明)

    # vim /etc/default/grub
    ## add hugepagesz=2M transparent_hugepage=never
    ...
    GRUB_CMDLINE_LINUX_DEFAULT="... hugepagesz=2M transparent_hugepage=never ..."
    ...

產生grub.cfg

    # grub-mkconfig -o /boot/grub/grub.cfg

### 時間
不是那麼重要，時間只要差異在二十四小時內，通常逛網頁之類的不會有太大的為難(會通容時差關係)。

    OPTS="$OPTS -rtc clock=host,base=localtime"

### 音效設定
常理說要export 變數，範例沒有所以多加上(反正不會壞就是了:P)。  
簡而言之雙系統的聲音會從Linux預設的喇叭播放。
[soundhw](http://wiki.qemu.org/download/qemu-doc.html)

    export QEMU_PA_SAMPLES=1024
    export QEMU_AUDIO_DRV=pa
    #OPTS="$OPTS -soundhw hda"
    OPTS="$OPTS -soundhw all"

### 顯示卡passthrough
    OPTS="$OPTS -device vfio-pci,host=01:00.0,multifunction=on"
    OPTS="$OPTS -device vfio-pci,host=01:00.1"

### BIOS部份
    # OVMF
    OPTS="$OPTS -drive if=pflash,format=raw,readonly,file=/usr/share/edk2.git/ovmf-x64/OVMF_CODE-pure-efi.fd"
    OPTS="$OPTS -drive if=pflash,format=raw,file=/usr/share/edk2.git/ovmf-x64/OVMF_VARS-with-csm.fd"


### 安裝系統
請注意！安裝系統檔案(win10.iso)與被安裝的磁碟(/dev/sd[X])它們的device皆須相符合才能正確安裝，否則會導致安裝程式找不到磁碟的悲劇。  
如scsi-hd to scsi-hd！並不能scsi-hd to hda(會找不到磁碟安裝)。  
如果使用實體硬碟可以用UUID去固定位置(有些狀況下開機後sdX不一定是原本的)
ls -l /dev/disk/by-[id,label,uuid etc...]/SOMEWHERE

    scsi-hd to scsi-hd  
    hd[a] to hd[b]  
    virtio-scsi-pci to virtio-scsi-pci  

    # Windows 10 installer
    # 安裝光碟
    OPTS="$OPTS -drive id=disk2,if=none,cache=none,aio=native,format=raw,file=/home/SOMEONE/images/win10.iso"
    OPTS="$OPTS -device driver=virtio-scsi-pci,id=scsi2"
    OPTS="$OPTS -device scsi-hd,drive=disk2"

    # 安裝磁區、檔案或硬碟等等
    OPTS="$OPTS -drive id=disk1,if=none,cache=none,aio=native,format=raw,file=/dev/disk/by-id/ata-INTEL_SSDSC2BW120A4_PHDA42630000000GN"
    OPTS="$OPTS -device driver=virtio-scsi-pci,id=scsi1"
    OPTS="$OPTS -device scsi-hd,drive=disk1"

### Virtio driver CD-ROM
安裝win系列方式會缺少virtio驅動，請自行新增。
[Windows Virtio Drivers](https://fedorapeople.org/groups/virt/virtio-win/virtio-win.repo)

    OPTS="$OPTS -drive id=virtiocd,if=none,format=raw,file=/home/SOMEONE/images/virtio-win-0.1.110.iso"
    OPTS="$OPTS -device driver=ide-cd,bus=ide.1,drive=virtiocd"

### USB passthrough
滑鼠、鍵盤丟給qemu模擬的系統使用。

    #OPTS="$OPTS -usb -usbdevice host:1532:001a"
    #OPTS="$OPTS -usb -usbdevice host:1532:001b"
    #OPTS="$OPTS -usb -usbdevice host:1532:001c"

### 網路設定
bridge要先設定才能使用這段 [qemu-if{up,down}](https://wiki.archlinux.org/index.php/QEMU#Creating_bridge_manually)請參考網站加入qemu-ifup & qemu-ifdown並修改chown, chmod, visudo  

    OPTS="$OPTS -netdev tap,vhost=on,ifname=$VM,script=/etc/qemu_ifup,id=br0"
    OPTS="$OPTS -device virtio-net-pci,mac=52:54:00:11:22:33,netdev=br0"
    #OPTS="$OPTS -net nic,vlan=0,macaddr=52:54:00:00:00:01,model=virtio,name=net0"
    #OPTS="$OPTS -net bridge,vlan=0,name=bridge0,br=br0"

#### 簡易bridge設定
br0啟動會一併把eno1裝置給啟動，跟eno1直接啟動是不同的。

    # ifconfig eno1 down
    # brctl addbr br0
    # brctl addif br0 eno1
    # ip link set br0 up
    # dhcpcd br0
    ### 選填給予IP(for synergy)
    # ifconfig eno1 123.123.123.123


### 關閉非必要display

    OPTS="$OPTS -vga none"
    OPTS="$OPTS -serial null"
    OPTS="$OPTS -parallel null"

### 始幕式
    synergy -f &
    sudo cpupower -c 2-7 frequency-set -g performance
    sudo taskset -c 2-7 qemu-system-x86_64 $OPTS

    sudo cpupower -c 2-7 frequency-set -g performance
    sudo taskset -c 2-7 qemu-system-x86_64 $OPTS

### 閉幕式

    sudo cpupower -c 2-7 frequency-set -g powersave

## Synergy
win系列到1.5版(含？)後就變成付費版本了，所以可以找之前的免費版本，<s>或是用nightly不小心試用一下之類的？</s>

### 解決轉視角問題
Synergy server端勾選Use relative mouse moves(中文譯「滑鼠相對方式移動」..可能吧)  
當您想玩遊戲時請按下鍵盤上的Scroll Lock，就會把滑鼠鎖在當下的螢幕。

## Win10更新問題
大版本更新後重開機可能會出現BSOD。  
請安裝更新後直接關機(關機更新)，在開機將可以更新。

### AMD顯卡
如使用q35要另外hack參數
[hack](http://www.se7ensins.com/forums/threads/how-to-setup-a-gaming-virtual-machine-with-gpu-passthrough-qemu-kvm-libvirt-and-vfio.1371980/)

    <qemu:arg value="ioh3420,bus=pcie.0,addr=1c.0,multifunction=on,port=1,chassis=1,id=root.1" />
    <qemu:arg value="-device" />
    <qemu:arg value="vfio-pci,host=[GPU],bus=root.1,addr=00.0,multifunction=on,x-vga=on" />
    <qemu:arg value="-device" />
    <qemu:arg value="vfio-pci,host=[Audio],bus=root.1,addr=00.1" />
已成功！
# 參考連結
[qemu-kvm -audio-help](https://bugzilla.redhat.com/show_bug.cgi?id=477955)
