---
title: MG279Q
date: 2016-04-12 19:08:10
tags:
    - monitor
    - outofdate
---
在台灣AMD市占率低且擁有較好的配備的玩家少，發一些基本的訊息讓玩家們參考一下。  

## MG279Q Freesync 一二三

1. freesync == 35-90Hz
freesync僅能支援到35-90Hz。    
當然有玩家們有提出一些方法讓這款螢幕可以freesync可以達到144hz。  

2. FullScreen ONLY  
[注意]：視窗化、視窗化全螢幕皆無Freesync效果，有reddit的人提到freesync需要gpu完全控制螢幕才有效果，如果想做全螢幕以外的事情可能會不太合適。  
[注意]：Crimson 版本已經支援視窗化全螢幕！如果有時候有Bug請alt+tab切換一下！但你需要知道...有時候更新可能會失效  

3. Crimson & monitor both need to set on  
當螢幕的freesync先打開才能開Crimson裡面的freesync。  

4. monitor set 90Hz(144Hz) in windows

(Optional) Vsync == off & freesync == on  
quality vsync > freesync > off  
performance off > freesync > vsync
當然也可以vsync+freesync都開，非頂級顯卡不建議打開，像我r9 390有freesync取代off就很感恩了。  
另外低畫格速率補償(LFC)有沒有效果...我是不知道啦(汗顏)。  

Crimson 遊戲 - > 全域設定 - > 等待垂直重新整理 -> 增強同步  
此設定介於效能與畫質的中間(建議開)，預見詳請請自尋搜尋  

## Tweak
[CRU](http://www.monitortests.com/forum/Thread-Custom-Resolution-Utility-CRU?page=1)   

這裡有些建議調整31-110,35-110,57-144,60-144，那...為什麼不30-144？原因是面板不夠勇猛？  

[ICC](http://www.tftcentral.co.uk/articles/icc_profiles.htm#games)
套用ICC可以增加色彩準度...大概啦...  
使用後mg279q的對比有準確。  

另外...
如果螢幕沒有freesync的設定可以透過該程式新增freesync。詳細資訊請參考論壇內容。

### bugs
freesync設定如果超出35-90Hz會跳出無法啟動的提示框，實際上還是有用的（挑逗一下後面的點點確定看看）。  
