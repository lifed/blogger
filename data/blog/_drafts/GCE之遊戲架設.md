---
title: GCE之遊戲架設
date: 2017-06-16 10:44:50
tags: [cloud,minecraft]
---
前言
此部份專注於簡易遊戲架設的紀錄，不再詳述GCE的一些基本設定。  

# Minecraft伺服器
在此是用forge架設伺服器，版本1.12.2，沒有迎頭趕上最新的版本是因為mod與plugin尚未支援。

## 安裝java

export JAVA_HOME=/usr/lib/jvm/java-11-openjdk/
export PATH_TO_FX=/usr/lib/jvm/java-11-openjdk/lib/

運氣好的話！最後會顯示如下。Java請安裝1.8版的


    $ sudo add-apt-repository "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main"
    $ sudo apt-get update
    $ sudo apt-get install oracle-java8-installer
    $ java -version
    java version "1.8.0_131"
    Java(TM) SE Runtime Environment (build 1.8.0_131-b11)
    Java HotSpot(TM) 64-Bit Server VM (build 25.131-b11, mixed mode)

## 下載forge並啟動
    $ cd /mnt/mcs
    # http://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.10.2.html
    # 請自行右鍵copy載點並且選擇Installer而非Windows Installer...
    $ wget "http://forge.jar"
    $ java -jar ./forge-****-installer.jar --installServer
    $ java -jar ./forge-****-universal.jar nogui
    # 這時應該會得到一個訊息
    [16:47:41] [Server thread/INFO]: You need to agree to the EULA in order to run the server. Go to eula.txt for more info.
    $ vim ./eula.txt
    #By changing the setting below to TRUE you are indicating your agreement to our EULA (https://account.mojang.com/documents/minecraft_eula).
    #Mon Jun 19 16:47:41 UTC 2017
    eula=true #改成true
    # 跟著下面按就會存檔了
    [esc]:wq[enter]
    $ java -jar ./forge-****-universal.jar nogui

## 開port
開啟Google Cloud Platform -> 網路 -> 防火牆規則 -> 建立防火牆規則  
![mcport](http://i.imgur.com/T8MhtrS.jpg)
謹記！如果在編輯模式要按儲存！！！  

這樣MC的伺服器就架好了！(mod上的問題請自行處理)  

# 架設Don't starve together伺服器 (也許過期了)
[DST Server Wiki](http://dontstarve.wikia.com/wiki/Guides/Don%E2%80%99t_Starve_Together_Dedicated_Servers)
[DST Server Steam Guides](http://steamcommunity.com/id/ToNiO44/myworkshopfiles/?section=guides&appid=322330)
如果您是跟著此次架設，必須照著Steam guides這份架設伺服器的文章。  
您該知道現在DST有分主世界(Master)與Caves的差別，維基有稍微提到一點訊息請參閱。  

## 抓取DST Server
下載相依套件 -> 下載steamCMD -> 下載DST Server  

### 下載相依套件
    # dpkg --add-architecture i386
    # apt-get update
    # apt-get install lib32gcc1
    # apt-get install lib32stdc++6
    # apt-get install libcurl4-gnutls-dev:i386

### 下載SteamCMD與DST Server
如果您有詳閱steam的教學文會發現我有一部分跳過了，像是新增使用者並登入，此部份有一些權限風險問題，若相當注重安全性者請注意。  
    # 此位置是我Server掛載SSD的地方
    # !!!請注意該絕對路徑權限問題!!!
    # cd /mnt/mcs/steam
    # wget https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz
    # tar -xvzf steamcmd_linux.tar.gz

    # ./steamcmd.sh
    # login anonymous
    # force_install_dir /mnt/mcs/dst
    # app_update 343050 validate
    # quit

## 設定DST Server
### 生成DST伺服器資料
首先需要製作與執行兩個Scripts，讓它們自動生成資料。  

    // 請不要真的傻傻的打apple...
    // 如果真的這樣做了...看個鳥哥吧...
    $ cd /home/apple/
    $ vim ./dst_master.sh
    cd /mnt/mcs/dst/bin //必須存在
    // persistent_storage_root設定Server資料的位置(cave要設定一樣)
    // cluster可以不設定或自訂(cave要設定一樣)
    // 其他的參數不要問那麼多=.=
    ./dontstarve_dedicated_server_nullrenderer -console -persistent_storage_root /mnt/mcs/dst -cluster Dst -shard Master

    $ vim ./dst_cave.sh
    cd /mnt/mcs/dst/bin/
    ./dontstarve_dedicated_server_nullrenderer -console -persistent_storage_root /mnt/mcs/dst -cluster Dst -shard Caves

當你看到Your Server Will Not Start的字眼，請按Ctrl+C，這代表伺服器資料已經生成完成但有部份還沒有設定所以伺服器並不會啟動。  

    // 兩個scripts都要執行過一次
    $ sh ./dst_master.sh
    [ctrl+c]
    $ sh ./dst_cave.sh
    [ctrl+c]

### 伺服器設定
現在應該知道的事情  
/mnt/[somewhere]/dst/DoNotStarveTogether/[clustername]
之後都會在這裡打滾設定，如果不清楚資料位置請釐清後在設定。  
沒有的文檔請自行新增！  
#### 金鑰
開啟遊戲取得金鑰  
![key](http://i.imgur.com/h41tUQh.jpg)

    $ vim /mnt/[somewhere]/dst/DoNotStarveTogether/[clustername]/cluster_token.txt
    貼上剛剛複製的金鑰
#### cluster.ini
    $ vim /mnt/[somewhere]/dst/DoNotStarveTogether/[clustername]/cluster.ini
    [GAMEPLAY]
    game_mode = survival
    max_players = 10
    pvp = false
    pause_when_empty = true

    [NETWORK]
    cluster_name = 誰誰誰的Server
    cluster_description = 誰誰誰的Server的伺服器描述
    cluster_password =
    cluster_intention = social
    autosaver_enabled = true
    enable_vote_kick = true
    whitelist_slots = 2
    tick_rate = 30

    [MISC]
    console_enabled = true

    [SHARD]
    shard_enabled = true
    bind_ip = 127.0.0.1
    master_ip = 127.0.0.1
    master_port = 11001
    cluster_key = dstyaya

#### 地圖設定
若須微調請自行參考檔案內容
##### Master
###### server.ini
    $ vim /mnt/[somewhere]/dst/DoNotStarveTogether/[clustername]/Master/server.ini
    [NETWORK]
    server_port = 10999

    [SHARD]
    is_master = true

    [STEAM]
    master_server_port = 12346
    authentication_port = 12345

    [ACCOUNT]
    encode_user_path = true

###### 地圖生成
[MSTER設定檔](https://drive.google.com/file/d/0B5YIt5r_JM3rcXRWb0tGQmdST1U/view?usp=sharing)  

    $ vim /mnt/[somewhere]/dst/DoNotStarveTogether/[clustername]/Master/worldgenoverride.lua

##### Caves
###### server.ini
    $ vim /mnt/[somewhere]/dst/DoNotStarveTogether/[clustername]/Caves/server.ini
    [NETWORK]
    server_port = 11000

    [SHARD]
    is_master = false
    name = Caves
    id = 2328533902

    [STEAM]
    master_server_port = 12348
    authentication_port = 12347

    [ACCOUNT]
    encode_user_path = true

###### 地圖生成
[CAVE設定檔](https://drive.google.com/file/d/0B5YIt5r_JM3rMnd1V0JpX3REOVE/view?usp=sharing)  

    $ vim /mnt/[somewhere]/dst/DoNotStarveTogether/[clustername]/Caves/worldgenoverride.lua

## 執行伺服器
可以先執行master先加入遊戲看看，地穴尚未啟動而找到該地方，則會出現障礙物阻擋你下去，之後找到地穴在啟動Cave。(遊戲進行中可啟動Caves)  

    $ sh ./dst_master.sh
    $ sh ./dst_cave.sh

## 疑難雜症
你應該知道的事情！
每張地圖(Caves, Master or custom)皆僅吃一顆CPU與RAM吃固定1G(未驗證)

### lag
1. 人數太多
2. 與太多生物交戰
3. 伺服器天數(無盡模式)
4. mod

### 忽然Server不能啟動
請先更新一下DST Server再啟動看看。

# Google Compute Engine free tier
目前是架一個proxy，雖然租用不用錢，但是網路流量的部份是需要錢的！  
之後依然會持續用，因為孤狗的網路就四快！(種花的替代品)  
![pay](https://i.imgur.com/rvaSxK7.png)  



# 設定Linux
## 掛載空間
現在的硬碟有HDD與SSD，那HDD是系統空間，還有申請一個SSD尚未掛載，現在就來把它們開機就掛載吧！

### 格式化
新的SSD需要先做格式化。  

    # 這裡申請的ssd ID叫mcdisk。那您申請的就自行[TAB][TAB]囉
    $ sudo cfdisk /dev/disk/by-id/google-mcdisk
    New -> 10G
    Type -> linux filesystem
    Write
    Quit

    # 請注意這裡是google-****-part1，代表的你剛剛分割的磁區。
    $ sudo mkfs.ext4 /dev/disk/by-id/google-mcdisk-part1

### 掛載
    # 手動掛載，如果想立即使用。(每次重開機都要自行掛載)
    $ sudo mkdir /mnt/mcs
    $ sudo mount /dev/disk/by-id/google-mcdisk-part1 /mnt/mcs/

    # 自動掛載（設定一次重開機都會自行掛載）
    # 查詢該磁碟的位置
    $ ls -lh /dev/disk/by-id/google-mcdisk-part1
    lrwxrwxrwx 1 root root 10 Jun 19 16:19 /dev/disk/by-id/google-mcdisk-part1 -> ../../sdb1
    # 查詢UUID
    $ sudo blkid /dev/sdb1
    /dev/sdb1: UUID="fc4727a5-0000-0000-891a-eaec17bee5b4" TYPE="ext4"
    $ sudo vim /etc/fstab
    # 已預先掛載
    UUID=b8a49195-00000000000000-c04bba97b797 / ext4 defaults 1 1
    # discard是針對SSD的參數只是...就用吧XD
    UUID=fc4727a5-0000-0000-891a-eaec17bee5b4 /mnt/mcs ext4 defaults,discard 1 1
    # swap請自行Google
    # 掛載fstab所設定的所有硬碟
    # sudo mount -a

若想掛載[swap](http://linux.vbird.org/linux_basic/0230filesystem.php#swap)請參考鳥哥的教學網站

## 權限設定
掛載的/mnt/mcs的路徑上有權限問題，有一些解決的方式。  
1. 簡單解法萬用sudo，凡是root執行，執行程式前sudo sh 123.sh
2. 複雜一點就修改chown|chmod
這裡就不詳細說明了，...好啦我以後有時間有心情在改啦(我好懶)。  
