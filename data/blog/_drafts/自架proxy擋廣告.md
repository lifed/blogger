---
title: 自架proxy擋廣告
date: 2017-12-19 11:49:37
tags: [cloud,network]
---
# 前言
最終目標是掛vpn就可以擋廣告。<s>大坑、崩潰</s>  

不過現階段是掛prxoy稍微擋廣告(汗顏)。  

GCP: debian 9  
GCP firewall port 8118  

# 安裝

    ## 安裝必要軟體
    $ sudo apt-get install git nginx privoxy

## 下載binary
[adblock2privoxy 下載頁面](https://projects.zubr.me/wiki/adblock2privoxyDownloads)    

    $ wget https://s3.amazonaws.com/ab2p/adblock2privoxy_1.4.2_amd64.debian8.deb
    $ sudo dpkg -i adblock2privoxy_1.4.2_amd64.debian8.deb
    $ sudo adblock2privoxy ...

### 編譯adblock2privoxy(optional)

    ## 安裝stack編譯器
    $ curl -sSL https://get.haskellstack.org/ | sh

    $ git clone https://github.com/essandess/adblock2privoxy
    $ cd adblock2privoxy/adblock2privoxy

    ## recommend
    $ sudo -E bash -c 'export PATH=/usr/bin:$PATH ; export STACK_ROOT=/path/to/local/stack/dir/without/spaces/.stack ; stack setup --allow-different-user ; stack install --local-bin-path /usr/local/bin --allow-different-user'

    ## or this one
    $ stack setup
    $ stack build
    $ sudo stack exec adblock2privoxy ...

# 各軟體設定
現在您應該要有privoxy, nginx, adblock2privoxy。

## nginx
編輯/etc/nginx/nginx.conf並在http區塊裡新增以下區塊，或者參考[nginx.conf](https://github.com/essandess/adblock2privoxy/blob/master/nginx.conf)。  

    server {
          listen 80;
          #ab2p css domain name (optional, should be equal to --domainCSS parameter)
          server_name DDNS(or IP);

          #root = --webDir parameter value
          root /var/www/privoxy;

          location ~ ^/[^/.]+\..+/ab2p.css$ {
              # first reverse domain names order
        rewrite ^/([^/]*?)\.([^/.]+)(?:\.([^/.]+))?(?:\.([^/.]+))?(?:\.([^/.]+))?(?:\.([^/.]+))?(?:\.([^/.]+))?(?:\.([^/.]+))?(?:\.([^/.]+))?/ab2p.css$ /$9/$8/$7/$6/$5/$4/$3/$2/$1/ab2p.css last;
          }

          location ~ (^.*/+)[^/]+/+ab2p.css {
              # then try to get CSS for current domain
              # if it is unavailable - get CSS for parent domain
              try_files $uri $1ab2p.css;
          }
    }

    $ sudo systemctl enable nginx
    $ sudo systemctl start nginx
    ## 測試網頁是否成功，應該能從亂碼堆看出架設成功的訊息
    $ curl 127.0.0.1


## adblock2privoxy
擋廣告清單請自行新增需求。  
網址有+號會導致錯誤，adblock2privoxy的問題，解決的方法有..  

1. 不要使用有+號的網址
2. 用別的下載器(wget, curl etc..)下載後在用adblock2privoxy整合

    $ sudo adblock2privoxy \
    -p /etc/privoxy \
    -w /var/www/privoxy
    -d DDNS \
    -t ab2p.task \
    https://easylist.to/easylist/easyprivacy.txt \
    https://github.com/cjx82630/cjxlist/blob/master/cjx-annoyance.txt  \
    https://easylist.to/easylist/fanboy-annoyance.txt \
    https://easylist.to/easylist/fanboy-social.txt  \
    https://easylist-downloads.adblockplus.org/antiadblockfilters.txt \
    https://easylist-downloads.adblockplus.org/malwaredomains_full.txt \
    https://easylist-downloads.adblockplus.org/easylistchina.txt \
    https://easylist-downloads.adblockplus.org/easylist.txt \
    https://raw.githubusercontent.com/Dawsey21/Lists/master/adblock-list.txt

## privoxy
代表任何人(:8118)都可以用此proxy  

    $ sudo vim /etc/privoxy/config

    ...
    actionsfile ab2p.system.action
    actionsfile ab2p.action
    filterfile ab2p.system.filter
    filterfile ab2p.filter

![conf](https://i.imgur.com/ZmUVDJ2.png)

    $ sudo systemctl enable privoxy
    $ sudo systemctl start privoxy
    $ 該終端機掛proxy
    $ export http_proxy="localhost:8118"
    ## 測試網頁是否成功，應該能從亂碼堆看出架設成功的訊息
    $ curl p.p
    $ export http_proxy=

# 使用proxy
使用後p.p應該會出現的畫面
![p.p](https://i.imgur.com/aHApzFW.png)

這樣應該能阻擋一些手機廣告的存在。  

再來是要繼續填的[坑](https://github.com/essandess/easylist-pac-privoxy)
