---
title: pci passthrough 3
date: 2018-06-27 18:29:58
tags:
    - qemu
---
前言  
對...又開了一個新篇幅，從第一篇的下指令，第二篇的圖形，再來本篇的效能設定。  
其實也沒什麼好講的就好像那樣子？本來計畫是ryzen組好來講一篇，一直在等等等等等等等等等！   

# 硬碟
[archwiki](https://wiki.archlinux.org/index.php/PCI_passthrough_via_OVMF#Physical_Disk.2FPartition)
[QEMU/libvirt SATA disk performance](https://forum.level1techs.com/t/qemu-libvirt-sata-disk-performance/124115/2)
這裡有幾種設定方案選擇

1. virtio
2. virtio-scsi + scsi
3. virtio with block
4. virtio-scsi + scsi with block
5. virtio-scsi + scsi with block + iothreads | !@$#%^*

## virtio & virtio-scsi + scsi
這兩種方式只要virtmanager介面按一下就差不多了，如果要直接丟硬碟過去得記得使用virsh改。  
記得確認一下devices是否有標記到scsi的磁碟。  
 ![scsi](https://i.imgur.com/XVhzaKi.png)

## "virtio" | "virtio-scsi + scsi" with block
此部份請把圖形介面的硬碟刪除！「只用手動文字介面新增的方式」直接輸入下面這一段

      // !!!以下address設定不對 請自行修改未衝突之位置
      // virtio disk
      <disk type='block' device='disk'>
        <driver name='qemu' type='raw' cache='none' io='native'/>
        <source dev='/dev/disk/by-id/SOMEWHERESSD'/>
        <target dev='vda' bus='virtio'/>
        <address type='pci' domain='0x0000' bus='0x02' slot='0x0a' function='0x0'/>
      </disk>

      //  scsi disk
      <disk type='block' device='disk'>
        <driver name='qemu' type='raw'/>
        <source dev='/dev/disk/by-id/SOMEWHERESSD'/>
        <target dev='sda' bus='scsi'/>
        <address type='drive' controller='0' bus='0' target='0' unit='0'/>
      </disk>

      // virtio-scsi這段可以用圖形介面新增但仍須修改 driver這行
      // $vcpu=虛擬機cpu的數量
      <controller type='scsi' index='0' model='virtio-scsi'>
        <driver queues='$vcpu'/>
        <address type='pci' domain='0x0000' bus='0x0b' slot='0x00' function='0x0'/>
      </controller>

## virtio-scsi + scsi with block + iothreads | !@$#%^*
現在我的設定值是...

      ...
      <vcpu placement='static'>6</vcpu>
      <iothreads>1</iothreads>
      <cputune>
        <vcpupin vcpu='0' cpuset='1'/>
        <vcpupin vcpu='1' cpuset='2'/>
        <vcpupin vcpu='2' cpuset='3'/>
        <vcpupin vcpu='3' cpuset='5'/>
        <vcpupin vcpu='4' cpuset='6'/>
        <vcpupin vcpu='5' cpuset='7'/>
        <emulatorpin cpuset='0,4'/>
        <iothreadpin iothread='1' cpuset='0,4'/>
      </cputune>
      ...
      <devices>
        <emulator>/usr/sbin/qemu-system-x86_64</emulator>

        //discard = ssd trim? 未驗證

        <disk type='block' device='disk'>
          <driver name='qemu' type='raw' cache='writeback' io='threads' discard='unmap'/>
          <source dev='/dev/disk/by-id/ata-CT500MXSSD'/>
          <backingStore/>
          <target dev='sda' bus='scsi'/>
          <alias name='scsi0-0-0-0'/>
          <address type='drive' controller='0' bus='0' target='0' unit='0'/>
        </disk>
        ...

        // 有6顆cpu 所以queues給6，也可以給少一點好像沒什麼影響...
        // iothread設定 ssd 4k跑分微微上升
        <controller type='scsi' index='0' model='virtio-scsi'>
          <driver queues='6' iothread='1'/>
          <alias name='scsi0'/>
          <address type='pci' domain='0x0000' bus='0x05' slot='0x00' function='0x0'/>
        </controller>
        ...

跑分是...
mx500 500g + writeback + threads + iothreadpin 0-1,4-5  
![cpu0-1,4-5](https://i.imgur.com/Hi7EFce.png)
mx500 500g + writeback + threads + iothreadpin 0,4  
![cpu0,5](https://i.imgur.com/BEBrZqn.png)
這跑分變動很大，數值上不用太在意。(眼紅在意中...ˋ—ˊ)  

# 顯卡
## 虛擬機2D效能疑慮
簡單來講CPU的微碼補丁導致分數疑慮
[bad performance](https://www.reddit.com/r/VFIO/comments/97unx4/passmark_lousy_2d_graphics_performance_on_windows/)
[how fix](https://heiko-sieger.info/low-2d-graphics-benchmark-with-windows-10-1803-kvm-vm/)
