---
title: 'PCI passthrough 2'
date: '2016-08-26 22:56:12'
tags: ['技術', '過期']
draft: false
---
Quick Links
[PCI passthrough via OVMF](https://wiki.archlinux.org/index.php/PCI_passthrough_via_OVMF)  
[libvirt](https://wiki.archlinux.org/index.php/libvirt)
[Virtio driver changelog](https://fedorapeople.org/groups/virt/virtio-win/CHANGELOG)

2020.03
virtmanager有xml可以編輯可以不太用依靠終端機的編輯了！
不過因為工作的關係比較少碰觸這樣的架構
以下步驟可以參考但已接近out of date!

## 系統環境
### 條件
1. 兩張顯示卡(內顯、外顯｜外顯、外顯)
2. 在BIOS開啟虛擬化技術
3. 強制HOST顯卡開機

### 實做配備
cpu: i7-4790  
ram: 24g  
gpu: hd4600(host), <s>r9-390(guest)</s>, rx-570(guest)  
OS: Manjaro(arch)  
kernel: 5.2以上  
power: 650w
請注意這裡的「電源供應器」瓦數是否足夠，像550w無法同時推動r9-390與hd4600！  
瓦數不夠的狀況下，某些高負載環境下會導致電源供應器出現過載後會發生的狀況(如：無預警重開機)。

## 軟體安裝
    ### GUI與qemu
    ## pacman -S qemu virt-manager
    ### libvirt相依套件
    ## pacman -S libvirt ebtables dnsmasq bridge-utils
    ### UEFI for qemu
    ## yay -S ovmf-git

### grub設定

   ```text
    ### Enabling IOMMU
    ### amd的cpu有另類參數請相對查閱
    ## vim /etc/default/grub
    GRUB_DISTRIBUTOR="Manjaro"
    GRUB_CMDLINE_LINUX_DEFAULT="... intel_iommu=on ..."

    ### isolate GPU
    ### 意味著讓虛擬機可以完全獨占GPU不受linux端的影響
    ## lspci -nn
    GRUB_CMDLINE_LINUX_DEFAULT="... vfio-pci.ids=1??2:6??1,1??2:a??8 ..."

    ### isolcpus參數
    GRUB_CMDLINE_LINUX_DEFAULT="... isolcpus=2,3,6,7 ..."

    ### hugepage
    ### 虛擬機的記憶體=1G*X
    ### 如要給12G X=12
    default_hugepagesz=1G hugepagesz=1G hugepages=X

    ### 產出新的grub.cfg
    ## grub-mkconfig -o /boot/grub/grub.cfg
    ```

做完即可重開機。

#### 驗證

    ## dmesg | grep -i vfio
    [    0.329224] VFIO - User Level meta-driver version: 0.3
    [    0.341372] vfio_pci: add [10de:13c2[ffff:ffff]] class 0x000000/00000000
    [    0.354704] vfio_pci: add [10de:0fbb[ffff:ffff]] class 0x000000/00000000
    [    2.061326] vfio-pci 0000:06:00.0: enabling device (0100 -> 0103)


## libvirt 設定
[libvirt wiki](https://wiki.archlinux.org/index.php/Libvirt)

### 虛擬機的BIOS設定
安裝ovmf-git請參考下面的位置，如果抓fedora的請自行給該rpm解壓縮的位置！(請注意「絕對路徑」的"權限問題")  
ovmf-git安裝位置是不一樣的請注意。

    ## vim /etc/libvirt/qemu.conf
    ...
    nvram = [
    	"/usr/share/ovmf/ovmf_code_x64.bin:/usr/share/ovmf/ovmf_vars_x64.bin"
    ]
    ...

### 權限設定
請自行修改USERNAME！

    ## vim /etc/polkit-1/rules.d/50-libvirt.rules

    polkit.addRule(function(action, subject) {
        if (action.id == "org.libvirt.unix.manage" &&
            subject.isInGroup("kvm")) {
                return polkit.Result.YES;
        }
    });

    ## gpasswd --add USERNAME kvm

    ## vim /etc/libvirt/libvirtd.conf
    ...
    unix_sock_group = "libvirt"
    unix_sock_ro_perms = "0777"  ## set to 0770 to deny non-group libvirt users
    unix_sock_rw_perms = "0770"
    auth_unix_ro = "none"
    auth_unix_rw = "none"
    ...
    ## gpasswd --add USERNAME libvirt

### 啟用

    ## systemctl enable libvirtd
    ## systemctl enable virtlogd
    ## systemctl start libvirtd
    ## systemctl start virtlogd

#### 驗證

    $ virsh -c qemu:///system
    Welcome to virsh, the virtualization interactive terminal.

    Type:  'help' for help with commands
           'quit' to quit

如果這裡驗證失敗，請尋找原因，因為這裡失敗再做下一步並沒有任何意義。  
修改設定檔後記得restart！

## virtmanager

### 參數設定
減少win10 BSOD出現。

    ## vim /etc/modprobe.d/kvm-iommu.conf

    ## system service exception
    options kvm ignore_msrs=1

    ## 以下參數可能不需要了
    ## for intel gpu
    ## options i915 enable_hd_vgaarb=1

    ## 如果有BSOD可以嘗試加入下面參數
    ## options kvm allow_unsafe_assigned_interrupts=1
    ## options vfio_iommu_type1 allow_unsafe_interrupts=1

### 虛擬機
DOMAIN是指虛擬機名稱 如我是給win10就該鍵入win10。

    ## virsh edit DOMAIN

匯出虛擬機設定檔(無用)

    $ sudo virsh dumpxml win10 > ~/win10.xml

虛擬機網路啟動(未特別設定預設是default)

    ## virsh net-start default

自動啟動虛擬機網路

    ## virsh net-autostart default

請自行用virtmanager新增一個虛擬機出來，如下圖
硬碟部份是SSD的參數，非SSD請自行參悟！
![virt](https://i.imgur.com/0k5e9CR.jpg)

此時記憶體尚未使用hugepage但依然可以測試，測試時請調低記憶體使用量。  
如PCI Passthrough啟動成功，那就差不多了！


#### 虛擬績優化
[我的虛擬機範例](https://drive.google.com/file/d/1ff6-WwXc7QVuCIN671_IiLMXKVZlWgC5/view?usp=sharing)  
請自行尋找插入點

##### CPU
[CPU Tuning](https://libvirt.org/formatdomain.html)   
[guiperpt的vcpupin設定](https://www.reddit.com/r/VFIO/comments/4yg3dg/double_check_my_cpu_pinning_general_cpu_pinning/)

此部份有兩種設定方

1. cores * 1threads
2. cores * 2threads

第一種vcpu與cpuset應不必特別對應
第二種需要，amd(01 23 45 67)與intel的cpu(04 15 26 37)順序並不同，請查明後在進行設定。
效能可能有差異？有些遊戲會針對第二種設定提出效能警告。

    <cpu mode='host-passthrough'>
      <topology sockets='1' cores='4' threads='1'/>
    </cpu>

isolcpus=1,2,3,5,6,7  
emulatorpin可微調或取消。

    <cputune>
      <vcpupin vcpu='0' cpuset='1'/>
      <vcpupin vcpu='1' cpuset='2'/>
      <vcpupin vcpu='2' cpuset='3'/>
      <vcpupin vcpu='3' cpuset='5'/>
      <vcpupin vcpu='4' cpuset='6'/>
      <vcpupin vcpu='5' cpuset='7'/>
      <emulatorpin cpuset='3,7'/>
    </cputune>

iothreads與iothreadpin不用特地設定，除非您想要神優化。

##### RAM - hugepage

      <memoryBacking>
        <hugepages/>
      </memoryBacking>

##### vga passthrough
直接從virtmanager pass過去即可。

當vga已丟給虛擬機但是螢幕未顯示時，請新增「display spice & video cirrus」。

啟動後點選show graphical console，由此視窗進入windows並且安裝顯卡驅動，此時vga對應的螢幕端的應當能正確被驅動。

##### Disk
Disk bus = virtio
可使用/dev/disk/by-id/代替/dev/sdx，如果有許多硬碟、裝置會喜歡這麼做的！  
iothread='1' 如果上面的cpu有設定，那這裡就得加進去但"不建議"，微調不好肯定影響效能。

    <disk type='file' device='disk'>
      <driver name='qemu' type='raw' cache='none' io='native' iothread='1'/>
      <source file='/dev/disk/by-id/ata-INxxx_xxxxxxxxxxxxxxxx1L01207GN'/>
      <target dev='vda' bus='virtio'/>
      ...
    </disk>

    <disk type='file' device='cdrom'>
      <driver name='qemu' type='raw' cache='directsync'/>
      <source file='/home/somewhere/virtmanager/virtio-win-0.1.???.iso'/>
      <target dev='sda' bus='sata'/>
      ...
    </disk>

    <disk type='file' device='cdrom'>
      <driver name='qemu' type='raw' cache='directsync'/>
      <source file='/home/somewhere/virtmanager/Win10_2004_x64.iso'/>
      <target dev='sdb' bus='sata'/>
      ...
    </disk>

### looking glass
[looking glass](https://looking-glass.hostfission.com/quickstart)
可以用一個應用程式的視窗顯示並操控虛擬機(vga pass)，意味著你不用一條實體的線從顯卡拉到螢幕端，近乎毫無延遲的執行虛擬機。  
[不懂文字敘述請點我](https://www.youtube.com/watch?v=ChoTLcDs0XE)
應用層面請自行發揮想像力。    
舉例來說...  
在windows開netflix影片，在linux上用looking glass開discord直播。XDDDD (安捏母湯喔，好孩子勿學！)

## 疑難雜症
### (推薦)barrier
取代synergy的GUI同時也無須註冊。

#### Passing keyboard/mouse via Evdev
[Passing keyboard/mouse via Evdev](https://wiki.archlinux.org/index.php/PCI_passthrough_via_OVMF#Passing_keyboard/mouse_via_Evdev)

### amd driver 17.12.1 overlay
請詳閱內容可能有效能上的疑慮。  
[alt+r fix](https://forum.level1techs.com/t/amd-gpu-passthrough-radeon-software-adrenalin-edition-17-12-1/122355)

### 音效卡
#### (推薦)Scream - Virtual network sound card for Microsoft Windows
[Scream](https://github.com/duncanthrax/scream)  
若要更多的細項請參考github的README。  
目前依然使用Scream，完美。

##### win10
安裝驅動從releases上下載並用admin啟動bat。  
HKLM\SYSTEM\CurrentControlSet\Services\Scream\Options\UseIVSHMEM  
登錄檔新增Options的機碼(資料夾)並新增DWORD並命名UseIVSHMEM數值為2

記得重開機讓登錄檔生效

##### linux
安裝接收器

    $ yay -S scream-pulse
    ## EDITOR=vim virsh edit [yourvmname]

    ...
            <shmem name='scream-ivshmem'>
                <model type='ivshmem-plain'/>
                <size unit='M'>2</size>
                <address type='pci' domain='0x0000' bus='0x00' slot='0x11' function='0x0'/>
            </shmem>
        </device>
    </domain>

    ### 啟動  
    $ scream-ivshmem-pulse /dev/shm/scream-ivshmem

#### ac97
首段必須新增參數，尾段才能加入hack參數。
此部份僅新增音效部份。

    <domain type='kvm' xmlns:qemu='http://libvirt.org/schemas/domain/qemu/1.0'>
    ...
    ...
    ...
      <qemu:commandline>
        <qemu:env name='QEMU_AUDIO_DRV' value='pa'/>
        <qemu:env name='QEMU_PA_SERVER' value='/run/user/1000/pulse/native'/>
      </qemu:commandline>
    </domain>

使用ac97並安裝該驅動。(不會安裝？請洽Youtube "ac97 driver win10")   
感覺音效太小聲？請開啟響度均勻

#### after qemu 3.0
[bad audio/fixing crackling](https://wiki.archlinux.org/index.php/PCI_passthrough_via_OVMF#With_qemu-patched)  
在qemu 3.0以後可以直接ich9且不會破音但是有時會有延遲。

1. 安裝qemu-patched
2. 新增graphics -> spice server
3. 編輯/etc/libvirt/qemu.conf加入nographics_allow_host_audio = 1
4. 移除hack參數有關於音效的部份
5. 開啟virtmanager請不要關閉(維持spice server開啟的狀態)

![like this](https://i.imgur.com/4FRbfiy.png)

如果搭配pulseeffects，可能會有聲音延遲的狀況(請開關virt與effect)。
#### after qemu 4.0
聽說4.0音效延遲會改善很多...
[qemu4.0](https://www.reddit.com/r/VFIO/comments/b1crpi/qemu_40_due_soon_might_bring_superb_audio_test_now/)
測試後大概是有點破音 延遲未知？
out.latency=20000｜in.latency=20000，會讓聲音跑比較快，So....

一點意外的插曲是...
qemu4.0 新增的虛擬機讓rx 570無法驅動 code 43

### win10重大更新
如果更新後無法正常重開機，請選擇「更新並關機」再開機！

### Samba & VHD
徹底解決網路磁碟執行程式會發生的問題。  
開啟samba磁碟(\\IP\galgame)-->在該目錄上建立虛擬磁碟(vhd)-->win系統掛載該磁碟
<iframe width="560" height="315" src="https://www.youtube.com/embed/labK2DjgBLU" frameborder="0"
        allowfullscreen></iframe>

請注意！使用虛擬網路會影響HOST and GUEST CPU效能！

### win10 微調
這部份是為了Win10卡頓所延伸的可惜都沒中獎

1. winkey + i 能關閉就關閉(像是手機、平板跳訊息出來那種)
2. 移除M$ $tore的所有軟體

第二點經歷了許久終於看到一個超棒棒的版本  
[Remove apps](https://community.spiceworks.com/scripts/show/3977-windows-10-decrapifier-version-2)
流程如下  
powershell用系統管理員 -> 鍵入 "Set-ExecutionPolicy -ExecutionPolicy bypass" -> 執行Script.ps1

### Unknown header type 7f
未知原因  
臆測 主機板有問題..

#### R9 390
linux端有時可讀取到Unknown header type 7f，或無偵測此VGA。
無法開啟至BIOS畫面，推斷可能顯卡已故障。
#### GTX 750
正常運作。。
！無詳細測。
！無重開機。
#### RX 570。
虛擬機執行重開機時，系統整個當住十來秒並且觸發Unknown header type 7f，無法pass顯卡、虛擬機亦黑畫面。  
host端重開機可正常再讀取。
關機則無變化，但再次啟動時觸發Unknown header type 7f。

發生無規律  
20191010依然有發生機率但變小了。
