---
title: Routeros使用Container架設服務
date: 2023-01-23 13:06:09
tags: ['網路', '技術']
---
MikroTik設備必須是能支援Container，並且非常建議需要一個外接隨身碟。

測試過的服務有linuxserver/unifi-controller，但無法正常使用(請詳閱該區段)。    

<s>目前使用的服務有PiHole，正常運作中。  </s>

**使用routeros的container有可能損壞外接硬碟**
**使用routeros的container有可能損壞外接硬碟**
**使用routeros的container有可能損壞外接硬碟**

上述很重要的事項只是猜測，最近發現PiHole擋廣告又不太靈敏想說調整一下，才發現隨身碟又被操到異常，所以我很納悶。  

假設說您看到container出現error，且在Log看到failed to extract layer，大概可以換一個隨身碟試試看。  

您可能觀察Files會跑檔案，但是它最終沒有跑完就直接跳error，這時你可以先懷疑隨身碟又出問題了。  

已經壞兩個隨身碟，一個是真的壞了，另一個拿到PC小測試一下還可以傳輸資料，所以不太確定是哪方面的問題。  

真的壞是已經用超多年了，可以當古董的那種；新的隨身碟是超小隨身碟(USB在多出來0.5-1公分左右吧)SanDisk；現在使用的也是新的一般大小隨身碟(Kingston)，使用一陣子再看看吧。

另外快速辨別隨身碟是否異常，請登入PiHole，在廣告列表清(Adlists)單內隨便新增一筆，可以順利新增代表暫時沒問題。

Kingston的隨身碟目前也有異常，所以routeros的container暫時不使用了，也發現如果使用pihole的dns的反應速度不是那麼快，雖然會過濾廣告。  


## RouterOS
硬體：hAP ac^3(arm)  
謎之音：為什麼不買新款的？  
我：那時候還沒出Q_Q  
[Container PiHole](https://help.mikrotik.com/docs/display/ROS/Container)
照著參考文件操作基本上就ok囉(被毆)。  

### 準備
如果做不到任何一步可以上一頁囉。

1. Container package is compatible with arm arm64 and x86 architectures.
確認硬體設備是否支援
2. A container is MikroTik's implementation of Linux containers, allowing users to run containerized environments within RouterOS. The container feature was added in RouterOS v7.4beta4.
確認系統是否升級到7.4Beta4版本以後，目前只要裝stable 7版(7.7)

#### 隨身碟
請把外接隨身碟插到Router，請照下圖確認  
先看橘框從System->Disks可以確認您的隨身碟有沒有被讀取到
若您的外接隨身碟若需格式化請依需求格式化，如果隨身碟有要在Windows系統下使用，請用fat32否則fat32。   
如果您懂ext4那...懂唄。  
![check1](https://i.imgur.com/Q5KUjwL.jpg)

**拜託！不要把快要壞掉的隨身碟拿來使用。  
拜託！不要把快要壞掉的隨身碟拿來使用。  
拜託！不要把快要壞掉的隨身碟拿來使用。**  

**隨便買一個一兩百塊的隨身碟就很好用了！**

### 啟動routeros的container
執行後請重開RouterOS
```bash
/system/device-mode/update container=yes
```

### 網域
#### 建立虛擬裝置
可以自行設定name, address and gateway，後續關聯的部分則須自行修正。  

建立虛擬網卡(veth1)給Container使用
```bash
/interface/veth/add name=veth1 address=173.0.0.2/24 gateway=173.0.0.1
```
設定bridge給container並且新增該虛擬網卡(veth1)
```bash
/interface/bridge/add name=dockers
/ip/address/add address=173.0.0.0/24 interface=dockers
/interface/bridge/port add bridge=dockers interface=veth1
```
設定該網域的流量可以連出
```bash
/ip/firewall/nat/add chain=srcnat action=masquerade src-address=173.0.0.0/24
```

### Container
#### 環境變數
設定變數給PiHole使用。
```bash
/container/envs/add name=pihole_envs key=TZ value="Asia/Taipei"
/container/envs/add name=pihole_envs key=WEBPASSWORD value="mypwd"
/container/envs/add name=pihole_envs key=DNSMASQ_USER value="root"
```
請依照您的disk slot的顯示設定，通常是disk1但多次插拔會一直增加數值。    
資料夾才會正確被掛載，這是linux的觀念，所以不懂我也沒辦法XD。
```bash
/container/mounts/add name=etc_pihole src=disk1/etc dst=/etc/pihole
/container/mounts/add name=dnsmasq_pihole src=disk1/etc-dnsmasq.d dst=/etc/dnsmasq.d
```

#### 透過網路下載PiHole
設定PiHole要從哪個網站資料並抓到隨身碟的空間(disk1/pull)。
```bash 
/container/config/set registry-url=https://registry-1.docker.io tmpdir=disk1/pull
```
下載最新的PiHole至隨身碟空間
**如果空間不足會導致擷取或壓縮失敗，print會告訴表示error，但不會告訴您錯在哪裡**
```bash
/container/add remote-image=pihole/pihole:latest interface=veth1 root-dir=disk1/pihole mounts=dnsmasq_pihole,etc_pihole envlist=pihole_envs
```
可以透過print一直確認狀態
```bash
/container/print
```

#### 啟動該Container
start後面的數字是參考/container/print顯示資訊，如圖紅框所示
![print](https://i.imgur.com/H0ppgpc.jpg)
```bash
/container/start 0
```

#### Forward ports到內部的docker
這部分應該不用設定，您可以先試試網址列打上veth1的網路位置，會出現PiHole的轉跳登入提示。
若有問題請先照著設定。  
dst-address為routerIP  
to-addresses為vethIP  
若照著設定做，則在網址列上面打上routerIP。  

請確定PiHole可以正確連進去後，才往下面的步驟走，否則都是徒勞。  
```bash
/ip firewall nat
add action=dst-nat chain=dstnat dst-address=2.2.2.1 dst-port=80 protocol=tcp to-addresses=173.0.0.2 to-ports=80
```

#### 優化設定
依照您的設備給Ram的大小。  
```bash
/container/config/set ram-high=200M
```
設定該container開機時啟動，如果不小心斷電才不會導致找不到DNS的問題。  
```bash
/container/set 0 start-on-boot=yes
```
盡可能讓該container維持啟動狀態。
```bash
/container/shell 0
```
該container會輸出logging(winbox的左側選單有Log可以查看)
```bash 
/container/set 0 logging=yes
```

### DNS設定
要設定兩個地方  
1. Router請求DNS(橘色框)  
2. 用戶端請求DNS(綠色框)  

後續PiHole的紀錄才能正確紀錄用戶端IP位置，更容易釐清網路流量的來源。  
若依文章所示則是要設定173.0.0.2而非3.3.3.2。  

![dns](https://i.imgur.com/WF6Tt04.jpg)

## PiHole
### 設定DNS Server
DNS Server設定純屬個人偏好，請依自己需求而定。  
Interfaces settings請設定下方紅色區塊，反正這樣才有回應(不懂別問QQ)  
若真的有問題可以設定Permit all origins
![set1](https://i.imgur.com/paPpcRi.jpg)

接下來請刷新您的DNS。什麼不會？從Router端到用戶端都要刷新。什麼！不懂！？那怎麼有勇氣裝PiHole...  

### 刷新DNS
這樣刷新...請確保完全沒問題後才進行下面設定，否則都是徒勞。  
![flush](https://i.imgur.com/1Fpoq27.jpg)

### 阻擋清單
請找PiHole專屬的阻擋清單，不然會有很多錯誤。    
另外請避免使用過多清單會導致效能不佳影響網路速度，請考慮硬體設備的能耐。   
譬如說開Google首頁不是毫秒開可能設定上是有問題的。   

[Avoid The Hack: The Best Pi-Hole Blocklists (2023)](https://avoidthehack.com/best-pihole-blocklists)
![adlist](https://i.imgur.com/KqgU0cz.jpg)

### 啟動重力！(更新adlists資料)
![gravity](https://i.imgur.com/0wSiQun.jpg)

### 白名單
目前我只發現這個問題，當然依您的阻擋清單的設定，越複雜越有限制請注意。  
line影片：點擊失效會導致影片無法觀看。  
```bash
securepubads.g.doubleclick.net
```
![whitelist](https://i.imgur.com/0Jon60J.jpg)

### 更新PiHole
請把資料備份出來，砍掉Container再安裝一次，再把備份檔還原。  

您沒看錯就是這樣。

[How upgrade container?](https://forum.mikrotik.com/viewtopic.php?t=191640)

## [linuxserver/unifi-controller](https://hub.docker.com/r/linuxserver/unifi-controller)

1. CPU 64bit only
2. 記憶體必須足夠

若使用[32bit](https://info.linuxserver.io/issues/2022-05-31-unifi-controller/)只能下載該版本，其他的有測試過Log會提示已經不支援。

hAP ac^3是可以啟動但無法使用且會影響Router的效能，不建議使用。  

據我觀察，應該是記憶體不足導致的，啟動程式後會把Router的所有記憶體吃掉與CPU努力GC榨取更多記憶體，導致整體效能變差。  

若不死心的朋友們，可以自行測試看看，記得啟動Container前開啟Logging設定並使用Log查看訊息。  

所以...硬體太差請不要有大膽的想法。 :D