---
title: 眼鏡
date: 2023-11-12 22:47:29
tags: ["生活"] 
---
最近配了價位比較高的鏡片，分享一下<s>血淚史</s>。

## 前言
目前交手過的鏡片雖然從小到大很多款，但從自己選配後是選擇蔡司，沒有什麼特別的偏好，一直有蔡司的名聲在耳內環繞。  

對我來說鏡框能便宜就便宜，鏡片能好就好。

配眼鏡小細節是，最好白天去配會比較準確。  
## 鏡框
大多數人都會忽略這點，這次我也是忽略這點，但其實很重要對於臉大的人而已。  

1. 不會夾頭髮的設計
2. 臉型的大小
3. 眼睫毛的長度

第一點我有留長頭髮所以鏡框有特別挑一些不會夾頭髮的設計，因為原本這隻真的夾爆頭髮，脫掉的時候卡在縫縫裡一直扯到有夠難受。

第二點跟我無關，臉型很標準所以沒注重該問題，不過倒是有看到大臉男發生問題，特別註記一下。  
大致上是挑眼鏡最好框可以呈現**ㄇ字形**，如果帶起來**八字形**即便鏡框在好看也不適合您。

第三點是我眼睫毛長所以會刷到鏡片會導致鏡面油油的，所以挑選的時候反而要挑一些有鼻墊可以撐起來的。  

那您會問說店員不會注意到這些細節提醒嗎？當然...不會...除非您有特別品味（<s>刁</s>）。

那為什麼要先講鏡框？

**因為鏡片砸大錢卻毀在鏡框身上感覺會非常差。**  
**因為鏡片砸大錢卻毀在鏡框身上感覺會非常差。**  
**因為鏡片砸大錢卻毀在鏡框身上感覺會非常差。**  
## 蔡司
### 庫存片
通常一般人價位只會落在庫存片，其實沒什麼好講的，隨便帶隨便舒服。  
通常也不知道真正舒服明亮到底是什麼回事。  
美次戴新眼鏡店員總是會問，是不是很清楚很棒什麼牛鬼蛇神的讚美都跑出來。  
我都只冷冷的回，沒感覺。OS：啊幹，就真的沒感覺好像捧上天一樣。    

因為我是度數不足所以跑去配新的眼鏡，那會比較清晰是正常的，戴上去馬上可以感受到舒不舒服，我覺得是玄學啦。  

又不是盲人戴客製鏡片後，終於看清美麗世界的色彩的感動。  

對於其他客人戴上新眼鏡再講好清楚好像整個世界從480P變到4K的感受，個人是嗤之以鼻啦。

### 個人化鏡片
如果可以建議不要選，因為貴且後續調整比想像中的麻煩。  

個人戴上後沒有感覺很舒適，原因是原本眼鏡都可以亂戴。  

後來使用個人化鏡片，因為眼鏡參數調整太準，反而您不能戴的太隨便，不然會有很大的不舒服感，像我是頭暈。  

再者是假設鏡框挑很糟糕，那會毀了這個高端的鏡片，心痛的感覺喔...  

不過也因為我不想體驗這心痛的感覺，每個禮拜都會跑眼鏡行調整一下眼鏡讓其更服貼、更穩固。  

鏡片跟框我是沒什麼意見，老實說框跟鏡片1.67薄度剛剛好，挑的時候雖然心痛一點但還是滿意的。  

主要是鼻墊與框與耳朵勾著的地方調整最多，因為它不太能滑落，一定要在剛剛好的位置，對我來講暈眩感才不會那麼嚴重。    

所以如果要配記得找近一點的店家，我呢，調整一次來回車程都要花一個小時Orz。  

## 結論
如果下次再配會選個人化鏡片嗎？  
可能不會，我可能不需要這個酷東西，但更懂原來鏡框的調整並不是一兩次就可以很舒適。    

可能是我期望太高，反而滿失望的...

我已經去調整四次了吧！希望可以搞定QQ