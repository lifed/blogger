---
title: Tomcat兩三事
date: 2021-11-14 23:06:16
tags: ['技術']
---
那些年遇到的問題。
<!-- more -->

## Tomcat
[Changelog](https://tomcat.apache.org/tomcat-9.0-doc/changelog.html)

### 400 
問題發生版本:9.0.38(含)之後  
Fix: Improve the validation of entity tags provided with conditional requests. Requests with headers that contain invalid entity tags will be rejected with a 400 response code. Improve the matching algorithm used to compare entity tags in conditional requests with the entity tag for the requested resource. Based on a pull request by Sergey Ponomarev. (markt)  
問題：讀取網頁時常400
38版後對Header的異常字元會做檢查，假設Header傳入的字元有異常(像是"變成&#x22)，則會回傳400錯誤。    
38前因為資安問題，我們會把接收到的Header做一次編碼緊接著Tomcat再處理，但38後就不用處理這個問題了。    

### CPU 100%
問題發生版本:9.0.49之前  
Avoid possible infinite loop in OpenSSLEngine.unwrap when the destination buffers state is changed concurrently. (remm)  
[可能原因1](https://bz.apache.org/bugzilla/show_bug.cgi?id=64771)  
[可能原因2](https://bugs.openjdk.java.net/browse/JDK-8241054)  
問題：某個https客戶端不知道什麼原因很常CPU100%且當下無流量。    
追蹤方式使用jstack查找tomcat pid看thread卡在哪裡，後來指向OpenSSLEngine。

解決方式:更新版本tomcat與openjdk版本(未證實)

另外遠端查找此問題可能會很麻煩，事情發生得當下遠端主機的CPU持續100%，當Thread調度有問題的時候，可能會無法用jstack查找。   
之前我跟另外一位同事各負責一個環境，兩台都會偶發性的發生這種狀況，他的可以遠端進去勉強操作，我的則完全不行，可能要調降優先權等下次發生再查找。    

不確定後續問題有沒有修正(因為小弟已經離職了XD)。

### 10版衍生問題 
package javax.servlet.* - > jakarta.servlet.*  

不過官方有提供 [整合套件](https://github.com/apache/tomcat-jakartaee-migration) ，但小弟沒用過。  

其他套件捐獻給Jakarta也會有這類的問題，如：Javamail -> Jakarta Mail。    