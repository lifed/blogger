---
title: IntelliJ IDEA
date: 2021-04-07 21:33:13
tags: ['軟體']
---
本篇是圍繞Spring boot專案下作說明(如果有的話XD)。

## IntelliJ IDEA
您應該知道  

快捷鍵列表
```text
Help -> Keyboard Shortcut PDF
```
### 基礎
您該知道萬物的開始從搜尋開始(Search everywhere)，按兩下Shift。  

只要您知道關鍵字就可以嘗試從這裡開始。

#### Project structure(可搜尋)
您可以是著shift兩下，是著找出該畫面。

![Imgur](https://i.imgur.com/PeR9hCs.png)

當你從網路上clone一個專案，先下載SDK再設定Language level。

SDK基本上都是照專案的需求下載，macos記得要抓aarch64版本，才不會因為轉譯損失效能。

Language level通常不會高於SDK，不然撰寫程式的時候，IDE會推薦新穎的寫法，可能本機會編譯成功，但可能會導致後客戶端異常。 

假設客戶端Java沒有裝對應的JRE導致啟動異常。  

### 套件
看我開那麼多標籤根本在做死XD
#### Structure
可以幫助了解該

#### Build

#### Service

#### Database

#### Terminal

#### Git

#### Commit

### 外掛套件
搜尋安裝外掛套件的地方
![Imgur](https://i.imgur.com/KBAXr4D.png)

通常會裝一些專案需求的額外外掛，或是使IDE更方便的外掛。  
![Imgur](https://i.imgur.com/4GJOJTs.png)

#### Maven Helper(可搜尋)

![Imgur](https://i.imgur.com/zwVArEe.png)

這裡可以設定使用mvnd加速編譯。該mvnd應該是絕對路徑或者已經設定好環境變數。

可以看到Goals可以自行設定想要的執行的目標。

![Imgur](https://i.imgur.com/6nB7T31.png)

設定好了要怎麼使用呢？請搜尋"run maven goals"打對且找到正確的maven goal很重要。

![Imgur](https://i.imgur.com/Sgi1HQM.png)

註：什麼叫設定好環境變數？開啟任意終端機，打上mvnd可以正確被執行。

### 問題
#### linux系統輸入法位置不正確
[修正Java輸入框位置的Patch](https://github.com/prehonor/myJetBrainsRuntime)  

[整合JBR Source與Patch的JBR](https://github.com/RikudouPatrickstar/JetBrainsRuntime-for-Linux-x64)

若您已經自行修正或者使用他人好的JBR。

1. Shift快點兩下 -> Tab選擇Actions -> runtime
2. Ctrl + Shift + a -> runtime

選擇Choose boot java Runtime for the IDE

設定JBR的路徑，重開IDE使用輸入法時，游標應該會在正確位置上了。  

